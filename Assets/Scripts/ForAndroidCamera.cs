﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForAndroidCamera : MonoBehaviour
{
    public static bool m_bIsFocus;
    private bool flash_open = true;

    // Use this for initialization
    void Start()
    {
        m_bIsFocus = false;
        Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    // Update is called once per frame
    void Update()
    {
        //if (m_bIsFocus)
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0))
#elif UNITY_ANDROID || UNITY_IPHONE
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
#endif
        {
            Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }
    public void ChangeFlash()
    {
        Vuforia.CameraDevice.Instance.SetFlashTorchMode(flash_open);
        flash_open = !flash_open;
    }
}
